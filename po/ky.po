# Kirghiz translation for xdg-user-dirs
# Copyright (c) 2011 Rosetta Contributors and Canonical Ltd 2011
# This file is distributed under the same license as the xdg-user-dirs package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: xdg-user-dirs\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2013-06-27 10:18+0200\n"
"PO-Revision-Date: 2013-04-22 23:14+0000\n"
"Last-Translator: SimpleLeon <Unknown>\n"
"Language-Team: Kirghiz <ky@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2018-03-23 15:32+0000\n"
"X-Generator: Launchpad (build 18573)\n"

#: translate.c:2
msgid "Applications"
msgstr "Тиркемелер"

#: translate.c:2
msgid "applications"
msgstr "тиркемелер"

#: translate.c:3
msgid "Desktop"
msgstr "Иш столу"

#: translate.c:3
msgid "desktop"
msgstr "иш столу"

#: translate.c:4
msgid "Documents"
msgstr "Иш кагаздар"

#: translate.c:4
msgid "documents"
msgstr "иш кагаздар"

#: translate.c:5
msgid "Download"
msgstr "Жүктөө"

#: translate.c:5
msgid "download"
msgstr "жүктөө"

#: translate.c:6
msgid "Downloads"
msgstr "Жүктөөлөр"

#: translate.c:6
msgid "downloads"
msgstr "жүктөөлөр"

#: translate.c:7
msgid "Movies"
msgstr "Тасмалар"

#: translate.c:7
msgid "movies"
msgstr "тасмалар"

#: translate.c:8
msgid "Music"
msgstr "Музыка"

#: translate.c:8
msgid "music"
msgstr "музыка"

#: translate.c:9
msgid "Photos"
msgstr "Фотосүрөттөр"

#: translate.c:9
msgid "photos"
msgstr "фотосүрөттөр"

#: translate.c:10
msgid "Pictures"
msgstr "Сүрөттөр"

#: translate.c:10
msgid "pictures"
msgstr "сүрөттөр"

#: translate.c:11
msgid "Projects"
msgstr "Долбоорлор"

#: translate.c:11
msgid "projects"
msgstr "долбоорлор"

#: translate.c:12
msgid "Public"
msgstr "Жалпы"

#: translate.c:12
msgid "public"
msgstr "жалпы"

#: translate.c:13
msgid "Share"
msgstr "Бөлүшүү"

#: translate.c:13
msgid "share"
msgstr "бөлүшүү"

#: translate.c:14
msgid "Templates"
msgstr "Шаблондор"

#: translate.c:14
msgid "templates"
msgstr "шаблондор"

#: translate.c:15
msgid "Videos"
msgstr "Видеолор"

#: translate.c:15
msgid "videos"
msgstr "видеолор"
